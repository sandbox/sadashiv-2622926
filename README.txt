
CONTENTS OF THIS FILE
---------------------

 * Author
 * Description
 * Installation

AUTHOR
------
sadashiv (http://drupal.org/user/1773304)

DESCRIPTION
-----------
This module provides a field to upload documents directly to Google Drive
 and a formatter to display the videos.
 It uses the Google api php client library to interface the Google drive API
 and implements the "browser upload method" so the file never hits the Drupal
 file system. It saves storage and bandwidth and your users don't need a 
 Google Account. 
 
REQUIREMENTS
------------
Google api php client library
  https://github.com/google/google-api-php-client
A defined Google Application
  https://console.developers.google.com/project
Google CORS Upload file
  https://raw.githubusercontent.com/googledrive/cors-upload-sample/master/upload.js
Google Auth (Gauth)
  http://drupal.org/project/gauth


INSTALLATION
------------

1. Enable the module as usual in Administer -> Site building -> Modules.

2. Install 
- the Google API php client library
  download it here https://github.com/google/google-api-php-client
  Extract it so the path is
  sites/all/libraries/google-api-php-client/src/Google/autoload.php or
  sites/[domain]/libraries/google-api-php-client/src/Google/autoload.php
  is available.
  
- the Google CORS Upload file
  download it here
  https://raw.githubusercontent.com/googledrive/cors-upload-sample/master/upload.js
  Extract it so the path is
  sites/all/libraries/google-api-cors-upload/cors_upload.js or
  sites/[domain]/libraries/google-api-cors-upload/cors_upload.js is available.
  
CONFIGURATION
-------------

1. To get credentials,
 go to https://console.developers.google.com/project. Next in "APIs & auth" ->
 "Credentials" -> "OAuth".
 Click "Create new Client ID" and then under "Application Type" select
 "Web Application".
For the redirect uri use http://yourdomain/gauth/callback or
 copy it from gauth configuration page.

2. You will next have to authorise your application (by clicking adding a gauth
 account and authenticate).
 This should be done only one time.
 The required token will be automatically refreshed for any additional request.

3. Create a new google drive uploader field on any entity and
 select the authenticated account.
