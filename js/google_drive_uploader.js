/**
 * @file
 * Javascript to upload doc to google drive.
 */

(function ($) {
  'use strict';
  Drupal.behaviors.google_drive_uploader = {
    attach: function (context, settings) {
      var accessToken = settings.google_drive_uploader.access_token;

      /**
       * Google drive file uploader class.
       *
       * @constructor
       */
      var UploadFile = function () {
        this.uploadStartTime = 0;
      };

      UploadFile.prototype.ready = function (accessToken, object) {
        this.accessToken = accessToken;
        $('a.upload-togoogledrive').bind("click", this.handleUploadClicked.bind(this));
      };

      /**
       * Uploads a files to Drive.
       *
       * @method uploadFile
       * @param object file
       *   File {object} corresponding to the file to upload.
       * @param string accessToken
       *   Access {token} required to upload.
       */
      UploadFile.prototype.uploadFile = function (file, accessToken) {
        var uploader = new MediaUploader({
          file: file,
          token: accessToken,
          onComplete: function (data) {
            var uploadResponse = JSON.parse(data);
            // Add the id to the hidden fid field.
            $('input.google_drive_hidden_id:last').val(uploadResponse.id);
            $('input.google_drive_hidden_title:last').val(uploadResponse.title);
            // Remove form input value.
            $('.field-type-google-drive-upload .form-type-file input').val('');
            // Then trigger the real upload button so Drupal will rebuild everything fine.
            $('.field-type-google-drive-upload .upload-file').trigger("mousedown");
          }.bind(this),
          onError: function (data) {
            var message = data;
            try {
              var errorResponse = JSON.parse(data);
              message = errorResponse.error.message;
            }
            finally {
              alert(message);
            }
          }.bind(this),
          onProgress: function (data) {
            var currentTime = Date.now();
            var bytesUploaded = data.loaded;
            var totalBytes = data.total;
            // The times are in millis, so we need to divide by 1000 to get seconds.
            var bytesPerSecond = bytesUploaded / ((currentTime - this.uploadStartTime) / 1000);
            var estimatedSecondsRemaining = Math.round((totalBytes - bytesUploaded) / bytesPerSecond);
            var percentageComplete = Math.ceil((bytesUploaded * 100) / totalBytes);

            $('#upload-progress').attr({
              value: bytesUploaded,
              max: totalBytes
            });

            $('#percent-transferred').text(percentageComplete);
            $('#seconds-left').text(estimatedSecondsRemaining);

            $('.during-upload').show();
          }.bind(this)
        });
        // This won't correspond to the *exact* start of the upload, but it should be close enough.
        this.uploadStartTime = Date.now();
        uploader.upload();
      };

      // Create a fake button to send the file to Google Drive.
      if ($('.upload-togoogledrive').length < 1) {
        $('.field-type-google-drive-upload .form-file').after('<a href="javascript:;" class="button upload-togoogledrive">' + Drupal.t('Upload') + '</a>')
          .end().find('.upload-togoogledrive').bind("click", sendToGDrive);
      }

      function sendToGDrive() {
        // Remove error messages if any.
        $(this).parent('.form-managed-file').removeErrorMessage();
        $(this).unbind("click", sendToGDrive);

        var uploadFile = new UploadFile();

        uploadFile.uploadFile($('input.google-drive-uploader-file').get(0).files[0], accessToken);
        return false;
      }

      // Display error message.
      $.fn.showErrorMessage = function (message) {
        if ($(this).siblings('.messages').length < 1) {
          $(this).siblings('.messages').remove();
        }
        $(this).before('<div class="messages error" style="display:none">' + message + '</div>').siblings('.messages').slideDown();
      };
      $.fn.removeErrorMessage = function (message) {
        $(this).siblings('.messages').remove();
      };

      // Display throbber.
      $.fn.displayThrobber = function () {
        $(this).after('<div class="ajax-progress ajax-progress-throbber"><div class="throbber">&nbsp;</div><span>&nbsp;</span></div>');
      };

      $.fn.waitingMessage = function (message) {
        $(this).find('.ajax-progress span').text(message);
      };

      // Remove throbber.
      $.fn.removeThrobber = function () {
        $(this).siblings('div.ajax-progress').remove();
      };
    }
  };
})(jQuery);
