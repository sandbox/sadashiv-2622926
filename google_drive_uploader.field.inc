<?php
/**
 * @file
 * Implement a google_drive uploader field, based on file module's file field.
 *
 * @link https://github.com/googledrive/cors-upload-sample for implementation. @endlink
 *
 * @see js/google_drive_uploader.js
 */

/**
 * Implements hook_field_info().
 */
function google_drive_uploader_field_info() {
  return array(
    // We name our field as the associative name of the array.
    'google_drive_upload' => array(
      'label' => t('Google Drive Uploader'),
      'description' => t('Google drive uploader field'),
      'instance_settings' => array(
        'file_directory' => '',
      ),
      'settings' => array(
        'uri_scheme' => variable_get('file_default_scheme', 'public'),
        'display_field' => 0,
        'display_default' => 0,
      ),
      'default_widget' => 'google_drive_uploader_widget',
      'default_formatter' => 'google_drive_uploader_file_link',
    ),
  );
}

/**
 * Implements hook_field_widget_info().
 */
function google_drive_uploader_field_widget_info() {
  return array(
    'google_drive_uploader_widget' => array(
      'label' => t('Gdrive uploader'),
      'field types' => array('google_drive_upload'),
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'behaviors' => array(
        'multiple values' => FIELD_BEHAVIOR_CUSTOM,
        'default value' => FIELD_BEHAVIOR_NONE,
      ),
    ),
  );
}

/**
 * Implements hook_field_widget_form().
 */
function google_drive_uploader_field_widget_form(&$form, &$form_state, $field, $instance, $langcode, $items, $delta, $element) {
  $library = libraries_detect('googledrive-cors-upload');
  if (!$library['installed']) {
    drupal_set_message(t('Cors upload library is not loaded.  Please contact site administrator'), 'error');
  }

  $defaults = array(
    'display' => !empty($field['settings']['display_default']),
    'gdrive_hidden_id' => '',
  );
  $field_state = field_form_get_state($element['#field_parents'], $field['field_name'], $langcode, $form_state);
  // Don't know why the items are not populated... ?
  $field_state['items'] = !empty($form_state['values'][$field['field_name']][$langcode]) ? $form_state['values'][$field['field_name']][$langcode] : array();
  foreach ($field_state['items'] as $key => $itm) {
    if (empty($itm['fid'])) {
      unset($field_state['items'][$key]);
    }
  }
  if (!empty($field_state['items'])) {
    $items = $field_state['items'];
  }
  elseif (isset($form_state['values'][$field['field_name']])) {
    $items = array();
  }

  $element += array(
    '#type' => 'hidden',
    '#input' => TRUE,
    '#process' => array('google_drive_uploader_field_widget_process'),
    '#pre_render' => array('file_managed_file_pre_render'),
    '#extended' => TRUE,
  );

  if ($field['cardinality'] == 1) {
    // Set the default value.
    $element['#default_value'] = !empty($items) ? $items[0] : $defaults;

    $elements = array($element);
  }
  else {
    // If there are multiple values, add an element for each existing one.
    foreach ($items as $item) {
      $elements[$delta] = $element;
      $elements[$delta]['#default_value'] = $item;
      $elements[$delta]['#weight'] = $delta;
      $delta++;
    }
    // And then add one more empty row for new uploads except when this is a
    // programmed form as it is not necessary.
    if (($field['cardinality'] == FIELD_CARDINALITY_UNLIMITED || $delta < $field['cardinality']) && empty($form_state['programmed'])) {
      $elements[$delta] = $element;
      $elements[$delta]['#default_value'] = $defaults;
      $elements[$delta]['#weight'] = $delta;
      $elements[$delta]['#required'] = ($element['#required'] && $delta == 0);
    }

    // The group of elements all-together need some extra functionality
    // after building up the full list (like draggable table rows).
    $elements['#file_upload_delta'] = $delta;
    $elements['#theme'] = 'file_widget_multiple';
    $elements['#theme_wrappers'] = array('fieldset');
    $elements['#process'] = array('file_field_widget_process_multiple');
    $elements['#title'] = $element['#title'];
    $elements['#description'] = $element['#description'];
    $elements['#field_name'] = $element['#field_name'];
    $elements['#language'] = $element['#language'];
    $elements['#display_field'] = $field['settings']['display_field'];

    // Add some properties that will eventually be added to the file upload
    // field. These are added here so that they may be referenced easily through
    // a hook_form_alter().
    $elements['#file_upload_title'] = t('Add a new file');
    $elements['#file_upload_description'] = theme('file_upload_help', array('description' => '', 'upload_validators' => ''));
  }

  return $elements;
}

/**
 * Extra processing for our field.
 */
function google_drive_uploader_field_widget_process($element, &$form_state, $form) {
  $item = $element['#value'];
  $fid = isset($item['fid']) ? $item['fid'] : 0;

  $field = field_widget_field($element, $form_state);
  $instance = field_widget_instance($element, $form_state);
  $settings = $instance['settings']['google_drive_settings'];

  // Set some default element properties.
  $element['#file'] = $fid ? $fid : FALSE;
  $element['#tree'] = TRUE;

  $element['#theme'] = 'google_drive_uploader_field_widget';

  $cors_path = libraries_get_path('googledrive-cors-upload');
  if (!$cors_path) {
    drupal_set_message(t('upload.js file not found, check the !status-report section "Google Drive CORS Upload" for more information. Doocuments shall not be uploaded', array('!status-report' => l(t('Status Report'), 'admin/reports/status'))), 'error', FALSE);
  }
  // Attach library JS.
  $google_client = gauth_client_get($settings['google_drive_gauth_account'], FALSE);
  $token = $google_client->getAccessToken();
  /* // Attach library JS, module custom JS and setting to get access token */
  $element['#attached']['js'] = array(
    drupal_get_path('module', 'google_drive_uploader') . '/js/google_drive_uploader.js',
    array(
      'data' => array('google_drive_uploader' => array('access_token' => drupal_json_decode($token)['access_token'])),
      'type' => 'setting',
    ),
  );
  $element['#attached']['libraries_load'][] = array('googledrive-cors-upload');

  $element['fid'] = array(
    '#type' => 'hidden',
    '#value' => $fid,
    '#weight' => -17,
    '#attributes' => array('class' => array('google_drive_hidden_id')),
  );
  $element['title'] = array(
    '#type' => 'hidden',
    '#value' => isset($item['title']) ? $item['title'] : '',
    '#weight' => -17,
    '#attributes' => array('class' => array('google_drive_hidden_title')),
  );

  $ajax_settings = array(
    'path' => 'file/ajax/' . implode('/', $element['#array_parents']) . '/' . $form['form_build_id']['#value'],
    'wrapper' => $element['#id'] . '-ajax-wrapper',
    'effect' => 'fade',
  );

  $element['upload_button'] = array(
    '#name' => implode('_', $element['#parents']) . '_upload_button',
    '#type' => 'submit',
    '#value' => t('Upload'),
    '#validate' => array(),
    '#submit' => array('google_drive_uploader_field_widget_submit'),
    '#limit_validation_errors' => array($element['#parents']),
    '#ajax' => $ajax_settings,
    '#weight' => -5,
    '#attributes' => array(
      'class' => array('upload-file element-hidden'),
      'style' => array('display:none'),
    ),
  );

  $element['remove_button'] = array(
    '#name' => implode('_', $element['#parents']) . '_remove_button',
    '#type' => 'submit',
    '#value' => t('Remove'),
    '#validate' => array(),
    '#submit' => array('google_drive_uploader_field_widget_submit'),
    '#limit_validation_errors' => array($element['#parents']),
    '#ajax' => $ajax_settings,
    '#weight' => -5,
  );

  // The file upload field itself.
  $element['upload'] = array(
    '#name' => 'files[' . implode('_', $element['#parents']) . ']',
    '#type' => 'file',
    '#title' => t('Choose a file'),
    '#size' => 20,
    '#weight' => -10,
    '#suffix' => '<div class="during-upload" style="display: none"> <progress id="upload-progress" max="1" value="0"></progress> <span id="percent-transferred"></span>% <span id="seconds-left"></span>sec left </div>',
    '#attributes' => array(
      'class' => array('google-drive-uploader-file'),
    ),
  );

  if ($fid && $element['#file']) {
    $element['filename'] = array(
      '#type' => 'markup',
      '#markup' => '<div class="video-form-preview">' . $item['title'] . '</div>',
      '#weight' => -10,
    );
  }

  // Prefix and suffix used for Ajax replacement.
  $element['#prefix'] = '<div id="' . $element['#id'] . '-ajax-wrapper">';

  $element['#suffix'] = '</div>';

  $field = field_widget_field($element, $form_state);

  // Adjust the Ajax settings so that on upload and remove of any individual
  // file, the entire group of file fields is updated together.
  if ($field['cardinality'] != 1) {
    $parents = array_slice($element['#array_parents'], 0, -1);
    $new_path = 'file/ajax/' . implode('/', $parents) . '/' . $form['form_build_id']['#value'];
    $field_element = drupal_array_get_nested_value($form, $parents);
    $new_wrapper = $field_element['#id'] . '-ajax-wrapper';
    foreach (element_children($element) as $key) {
      if (isset($element[$key]['#ajax'])) {
        $element[$key]['#ajax']['path'] = $new_path;
        $element[$key]['#ajax']['wrapper'] = $new_wrapper;
      }
    }
    unset($element['#prefix'], $element['#suffix']);
  }

  foreach (array('upload_button', 'remove_button') as $key) {
    $element[$key]['#submit'][] = 'file_field_widget_submit';
    $element[$key]['#limit_validation_errors'] = array(
      array_slice($element['#parents'], 0, -1),
    );
  }

  return $element;
}

/**
 * Form submission handler for upload / remove buttons.
 *
 * @see file_managed_file_submit()
 */
function google_drive_uploader_field_widget_submit($form, &$form_state) {
  $parents = $form_state['triggering_element']['#array_parents'];
  $button_key = array_pop($parents);
  $element = drupal_array_get_nested_value($form, $parents);

  if ($button_key == 'remove_button') {
    // Need to get field settings.
    $field_info = field_info_instance($element['#entity_type'], $element['#field_name'], $element['#bundle']);
    $field_settings = $field_info['settings']['google_drive_settings'];

    // Remove the file from Drive.
    if (isset($element['fid']['#value']) && $field_settings['google_drive_uploader_delete'] != 0) {
      $google_client = gauth_client_get($field_settings['google_drive_gauth_account'], FALSE);
      $drive = new Google_Service_Drive($google_client);
      try {
        if ($field_settings['google_drive_uploader_delete'] == 1) {
          $response = $drive->files->trash($element['fid']['#value']);
          if ($response->explicitlyTrashed) {
            drupal_set_message(t('File moved to trash successfully'));
          }
          else {
            drupal_set_message(t("Can't find requested file. May be it's deleted."), 'error');
          }
        }
        else {
          $response = $drive->files->delete($element['fid']['#value']);
          drupal_set_message(t('File deleted from drive.'));
        }
      }
      catch (Exception $e) {
        drupal_set_message(t("Can't find requested file. May be it's deleted."), 'error');
      }
    }

    $values_element = $element['#extended'] ? $element['fid'] : $element;

    form_set_value($values_element, NULL, $form_state);
    drupal_array_set_nested_value($form_state['values'], $values_element['#parents'], NULL);
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Implements hook_field_is_empty().
 *
 * Return TRUE if it does not contain data, FALSE if it does. This lets
 * the form API flag an error when required fields are empty.
 */
function google_drive_uploader_field_is_empty($item, $field) {
  return empty($item['fid']);
}

/**
 * Implements hook_field_instance_settings_form().
 */
function google_drive_uploader_field_instance_settings_form($field, $instance) {
  $settings = isset($instance['settings']['google_drive_settings']) ? $instance['settings']['google_drive_settings'] : $instance['settings'];

  $gdrive_settings = array();
  if ($field['type'] == 'google_drive_upload') {
    $gauth_accounts = db_select('gauth_accounts', 'ga')
      ->fields('ga', array('name', 'id'))
      ->condition('ga.services', 'drive')
      ->condition('ga.is_authenticated', TRUE)
      ->execute()
      ->fetchAll();
    if (count($gauth_accounts)) {
      $options = array();
      foreach ($gauth_accounts as $account) {
        $options[$account->id] = $account->name;
      }
      $gdrive_settings['google_drive_gauth_account'] = array(
        '#type' => 'select',
        '#title' => t('Gauth account'),
        '#options' => $options,
        '#required' => TRUE,
        '#default_value' => isset($settings['google_drive_gauth_account']) ? $settings['google_drive_gauth_account'] : '',
      );
    }
    else {
      if (isset($settings['google_drive_gauth_account'])) {
        drupal_set_message(t("Account configured for this field is either deleted or modified and not authenticated again. Please check at <a href=@url target=_blank>here</a>", array('@url' => url('admin/config/services/gauth_account'))), 'error');
      }
      else {
        drupal_set_message(t("Can't find any authenticated gauth account authenticated for google drive. Please add account at <a href=@url target=_blank>here</a>", array('@url' => url('admin/config/services/gauth_account'))), 'error');
      }
    }

    $gdrive_settings['google_drive_uploader_delete'] = array(
      '#type' => 'radios',
      '#title' => t('Delete file from drive'),
      '#description' => t('Configuration for what should happen when user removes the file.'),
      '#options' => array(
        0 => t('Take no action'),
        1 => t('Move the file to Trash'),
        2 => t('Delete the file. Deletes file permanently'),
      ),
      '#default_value' => isset($settings['google_drive_uploader_delete']) ? $settings['google_drive_uploader_delete'] : FALSE,
    );

    // Check if cors library exists.
    $library = libraries_detect('googledrive-cors-upload');;
    if (!$library['installed']) {
      drupal_set_message(t('Cors upload library is not loaded.  Please check <a href=@url target=_blank>status report</a>', array('@url' => url('admin/reports/status'))), 'error');
    }
  }

  $form['google_drive_settings'] = $gdrive_settings;

  return $form;
}

/**
 * Implements hook_field_prepare_view().
 */
function google_drive_uploader_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, &$items) {
  file_field_prepare_view($entity_type, $entities, $field, $instances, $langcode, $items);
}

/**
 * Implements hook_field_prepare_view().
 *
 * Check if the field is required.
 */
function google_drive_uploader_field_validate($entity_type, $entity, $field, $instance, $langcode, $items, &$errors) {
  if ($instance['required'] == 1 && $items[0]['fid'] === "0") {
    form_set_error($field['field_name'], $instance['label'] . ' field is required. ');
  }
}

/**
 * Formatter.
 */

/**
 * Implements hook_field_formatter_info().
 */
function google_drive_uploader_field_formatter_info() {
  $formatters = array(
    // This formatter provides link to view/download the document.
    'google_drive_uploader_file_link' => array(
      'label' => t('Google Drive Document Link'),
      'field types' => array('google_drive_upload'),
      'settings' => array(
        'file_link_action' => 1,
      ),
    ),
  );

  return $formatters;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function google_drive_uploader_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  if ($display['type'] == 'google_drive_uploader_file_link') {
    $element['file_link_action'] = array(
      '#type' => 'select',
      '#title' => t('Link'),
      '#options' => array(1 => t('View in google drive'), 2 => t('Download the file')),
      '#default_value' => $settings['file_link_action'],
      /* '#empty_option' => t('None (original image)'), */
    );
  }

  return $element;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function google_drive_uploader_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  // Summary for the file links.
  if ($display['type'] == 'google_drive_uploader_file_link') {
    $actions = array(
      1 => t('View in google drive'),
      2 => t('Download the file'),
    );
    if (isset($settings['file_link_action'])) {
      $summary = t('Link will : @action', array('@action' => $actions[$settings['file_link_action']]));
    }
    else {
      $summary = t('Link will : View in google drive');
    }

    return $summary;
  }
}

/**
 * Implements hook_field_formatter_view().
 */
function google_drive_uploader_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();

  if ($display['type'] == 'google_drive_uploader_file_link') {
    foreach ($items as $delta => $item) {
      $element[$delta] = array(
        '#theme' => 'google_drive_uploader_file_link',
        '#file_id' => $item['fid'],
        '#title' => $item['title'],
        '#link_action' => isset($display['settings']['file_link_action']) ? $display['settings']['file_link_action'] : 1,
      );
    }
  }
  return $element;
}
